import 'package:flutter/material.dart';

// void main() {
//   runApp(gridview());
// }

class gridview extends StatelessWidget {

  // This widget is the root of your application
  @override
  Widget build(BuildContext context) {

    var arrColors =[Colors.red,
      Colors.orange,
      Colors.yellow,
      Colors.blue,
      Colors.green,
      Colors.pinkAccent,
      Colors.amber,
      Colors.brown];
    return MaterialApp(
      home: Scaffold(
        //backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Center(
            child: Text(
              'Flutter GridView ',
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            ),
          ),
        ),
        body: Container(
          height: 600,
          child: GridView.count(
            crossAxisCount: 3,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(color: arrColors[0],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(color: arrColors[1],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(color: arrColors[2],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(color: arrColors[3],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(color: arrColors[4],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(color: arrColors[5],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(color: arrColors[6],),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(color: arrColors[7],),
              ),
            ],
          ),
        ),
      ),
    );
  }
}