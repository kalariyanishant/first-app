import 'package:flutter/material.dart';

class Mycheckbox extends StatefulWidget {
  @override
  _MyHobbies createState() {
    return new _MyHobbies();
  }
}

class _MyHobbies extends State<Mycheckbox> {
  bool valuefirst = false;
  bool valuesecond = false;
  bool valuethird = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Hobbies '),
            ),
            body: Container(
              child: Column(
                children: <Widget>[
                  CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.trailing,
                    // subtitle: const Text('Game of enjoyment'),
                    title: const Text('Cricket'),
                    value: this.valuefirst,
                    onChanged: (value) {
                      setState(() {
                        this.valuefirst = value!;
                      });
                    },
                  ),
                  CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.trailing,
                    // subtitle: const Text('Game of Stamina'),
                    title: const Text('Kabaddi'),
                    value: this.valuesecond,
                    onChanged: (value) {
                      setState(() {
                        this.valuesecond = value!;
                      });
                    },
                  ),
                  CheckboxListTile(
                    controlAffinity: ListTileControlAffinity.trailing,
                    // subtitle: const Text('Brain Game'),
                    title: const Text('Watching Cricket'),
                    value: this.valuethird,
                    onChanged: (value) {
                      setState(() {
                        this.valuethird = value!;
                      });
                    },
                  ),

                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'gridview');
                    },
                    child: const Text('Grid view'),
                  ),

                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'Listview');
                    },
                    child: const Text('List View'),
                  ),

                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'Myradio');
                    },
                    child: const Text('Radio Button'),
                  ),

                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'Signup');
                    },
                    child: const Text('Signup Page'),
                  ),

                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'bottomnavbar');
                    },
                    child: const Text('Bottom Navigation Bar'),
                  ),

                  ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, 'pagination');
                    },
                    child: const Text('Pagination'),
                  ),

                ],
              ),
            ),

            ),
        );
    }
}