import 'package:Login/radiobutton.dart';
import 'package:flutter/material.dart';
import 'package:Login/Feature/Sign%20Up%20Screen/SignUp_Screen.dart';
import 'package:Login/bottomnavigationbar.dart';
import 'package:Login/checkbox.dart';
import 'package:Login/grid%20view.dart';
import 'package:Login/listview.dart';
//import 'package:Login/radiobutton.dart';

import 'Feature/Login Screen/Login_Screen.dart';
//import 'Feature/Sign Up Screen/SignUp_Screen.dart';
//import 'checkbox.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginScreen(),
      routes: {
        'checkbox':(context) => Mycheckbox(),
        'Myradio' :(context) => MyRadio(),
        'Signup' : (context) => SignupScreen(),
        'gridview' : (context) => gridview(),
        'bottomnavbar' : (context) => bottomnavbar(),
        'Listview': (context) => Listview(),

      },
    );
  }
}
