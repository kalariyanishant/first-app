import 'package:flutter/material.dart';

class MyRadio extends StatefulWidget {
  @override
  _MyRadio createState() {
    return new _MyRadio();
  }
}

class _MyRadio extends State<MyRadio> {
  String _gender = '';

  void _toggleGender(String value) {
    setState(() {
      _gender = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              title: const Text('Radio Button'),
            ),
            body: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Male'),
                Radio(
                  value: 'Male',
                  groupValue: _gender,
                  onChanged: (String? value) {
                    _toggleGender(''+ value!);
                  },
                ),
                Text('Female'),
                Radio(
                  value: 'Female',
                  groupValue: _gender,
                  onChanged: (String? value) {
                    _toggleGender(''+ value!);
                  },
                ),
              ],
            ),
            ),
        );
    }
}